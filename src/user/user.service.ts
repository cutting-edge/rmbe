import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, FindOneOptions, Repository } from 'typeorm';
import { UserAuthInfo } from './user.auth.info.interface';
import { User } from './user.entity';

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
      ) {}

    /**
     * Return all current users
     */
    async findAll(): Promise<User[]> {
        return this.userRepository.find();
    }

    /**
     * Return a user by their ID
     * @param id of the user to return
     * @param deep should be true to get the related instance, false leaves them out
     */
    async findOneById(id: number, deep: boolean = false): Promise<User | undefined> {
        let opts: FindOneOptions<User> = {};
        if (deep) {
            opts = {relations: ['shoppingList']};
        }
        return this.userRepository.findOne(id, opts);
    }

    async findByAuthInfo(userAuthInfo: UserAuthInfo): Promise<User | undefined> {
        return this.userRepository.findOne(
            {where: {
                username: userAuthInfo.username,
                password: userAuthInfo.password,
            }});
    }

    /**
     * Save a new user
     * @param user - the user to save or update
     */
    async create(user: User): Promise<User> {
        return this.userRepository.save(user);
    }

    /**
     * Update an existing user. Returns true of the user was
     * successfully updated and false if the user could not be
     * updated (because the key was missing or because it was not found)
     *
     * @param user user to update
     */
    async update(user: User): Promise<boolean> {
        if (this.userRepository.hasId(user)) {
            const foundUser: User | undefined = await this.findOneById(user.id);
            if (foundUser === undefined) {
                return false;
            }
            await this.userRepository.save(user);
            return true;
        }
        return false;
    }

    /**
     * Delete an existing user.  Note that we use the delete repository method
     * here. This translates directly into a delete on the database and does not
     * perform any checking.  This is suitable for users - probably not for other
     * entities.
     *
     * @param user - to remove
     */
    async deleteById(userId: number): Promise<DeleteResult> {
        return this.userRepository.delete(userId);
    }
}