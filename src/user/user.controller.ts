import { ParseIntPipe } from './../shared/parse-int.pipe';
import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    NotFoundException,
    Param,
    Post,
    Put,
    Query,
    UseGuards,
} from '@nestjs/common';

import { AuthGuard } from '@nestjs/passport';
import { ParseBooleanPipe } from './../shared/parse-boolean.pipe';
import { User } from './user.entity';
import { UserService } from './user.service';

@Controller('users')
@UseGuards(AuthGuard('jwt'))
export class UserController {
    constructor(private readonly userService: UserService) { }

    /**
     * return all users
     */
    @Get()
    async findAll(): Promise<User[]> {
        return this.userService.findAll();
    }

    /**
     * Return a user with the provided id
     *
     * @param params
     */
    @Get(':id')
    async findOneById(@Param('id', new ParseIntPipe()) id: number, @Query('deep', new ParseBooleanPipe()) deep: boolean): Promise<User> {

        const user: User | undefined = await this.userService.findOneById(id, deep);
        if (user === undefined) {
            throw new NotFoundException('Invalid user id');
        }
        return user;
    }

    /**
     * Create a new user
     *
     * @param user
     */
    @Post()
    @HttpCode(HttpStatus.CREATED)
    async create(@Body() user: User): Promise<User> {
        return this.userService.create(user);
    }

    /**
     * Updates the user passed or throws a BadRequetException if
     * the user is missing an id
     *
     * @param user user to be updated must have an id
     */
    @Put(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    async update(@Param('id', new ParseIntPipe()) id: number, @Body() user: User): Promise<void> {
        user.id = id;
        const updated = await this.userService.update(user);
        if (!updated) {
            throw new BadRequestException('Invalid user id');
        }
    }

    /**
     * Delete an existing user
     * @param params should contain the ID of the user to delete
     */
    @Delete(':id')
    @HttpCode(204)
    async deleteById(@Param('id', new ParseIntPipe()) id: number) {
        return this.userService.deleteById(id);
    }
}