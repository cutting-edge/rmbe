export interface UserAuthInfo {
    username: string;
    password: string;
}