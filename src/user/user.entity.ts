import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Ingredient } from '../ingredient/ingredient.entity';
import { Recipe } from '../recipe/recipe.entity';

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column('text')
    username!: string;

    @Column('text')
    firstName!: string;

    @Column('text')
    lastName!: string;

    @Column('text')
    emailAddress!: string;

    @Column('text')
    role!: string;

    @Column('text')
    password!: string;

    @OneToMany<Ingredient>(() => Ingredient, ingredient => ingredient.user)
    shoppingList!: Ingredient[];

    @OneToMany<Recipe>(() => Recipe, recipe => recipe.user)
    recipes!: Recipe[];
}
