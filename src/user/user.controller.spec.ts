import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Ingredient } from '../ingredient/ingredient.entity';
import { UserController } from './user.controller';
import { User } from './user.entity';
import { UserModule } from './user.module';

//
// Test the contents of a user instance
//
function testUser(user: User, id: number, username: string, firstName: string, lastName: string, emailAddress: string, role: string): void {
    expect(user.id).toBe(id);
    expect(user.username).toBe(username);
    expect(user.firstName).toBe(firstName);
    expect(user.lastName).toBe(lastName);
    expect(user.emailAddress).toBe(emailAddress);
    expect(user.role).toBe(role);
    expect(user.password).toBe('password123');
}

//
// Code to get the usercontroller from the app
//
function getUserController(app: TestingModule): UserController {
    return app.select<UserModule>(UserModule).get<UserController>(UserController);
}

describe('UserController', () => {

    let app: TestingModule;

    let saveId: number;

    beforeAll(async () => {
        app = await Test.createTestingModule({
            imports: [TypeOrmModule.forRoot(), UserModule],
        }).compile();
    });

    describe('findAll', () => {
        it('should return a list of 3 users', async () => {
            const userController = getUserController(app);
            const users: User[] = await userController.findAll();
            expect(users).toHaveLength(3);
            //
            // Test the users
            //
            testUser(users[0], 1, 'aandersen',  'Adam',   'Andersen',   'aandersen@ces.com', 'admin');
            testUser(users[1], 2, 'bbaxter',    'Bill',   'Baxter',     'bbaxter@ces.com',   'user');
            testUser(users[2], 3, 'cconnor',    'Cathy',  'Connor',     'cconnor@ces.com',   'user');
        });
    });

    describe('findOneById', () => {
        it('should return the user Adam', async () => {
            const userController = getUserController(app);
            const user: User = await userController.findOneById(1, false);
            testUser(user, 1, 'aandersen', 'Adam', 'Andersen',   'aandersen@ces.com', 'admin');

        });

        it('should return the user Adam and related instances', async () => {
            const userController = getUserController(app);
            const user: User = await userController.findOneById(1, true);
            testUser(user,  1, 'aandersen', 'Adam', 'Andersen',   'aandersen@ces.com', 'admin');
            const shoppingList: Ingredient[] = user.shoppingList;
            expect(shoppingList).toHaveLength(4);
            expect(shoppingList[0].name).toBe('brussel sprouts');
            expect(shoppingList[0].amount).toBe(40);
        });

        it('should fail on invalid user id', async () => {
            const userController = getUserController(app);
            try {
                const user: User = await userController.findOneById(999, false);
                fail('Valid user returned');
            }
            catch (error) {
                expect(error.status).toBe(HttpStatus.NOT_FOUND);
                expect(error.message.message).toBe('Invalid user id');
            }
        });
    });

    describe('create', () => {
        it('should create the new user Dave', async () => {
            const userController = getUserController(app);
            const user: User = new User();
            user.id = undefined;
            user.username = 'ddavis';
            user.firstName = 'Dave';
            user.lastName = 'Davis';
            user.emailAddress = 'ddavis@ces.com';
            user.role = 'admin';
            user.password = 'password123';
            const created: User = await userController.create(user);
            saveId = created.id;
            user.id = saveId;
            expect(created).toEqual(user);
        });
    });

    describe('delete', () => {
        it('should delete the created user', async () => {
            const userController = getUserController(app);
            await userController.deleteById(saveId);
            try {
                const user: User = await userController.findOneById(saveId, false);
                fail('created user not deleted');
            }
            catch (error) {
                expect(error.status).toBe(404);
                expect(error.message.message).toBe('Invalid user id');
            }
        });
    });

    describe('Update', () => {
        it('should update an existing user', async () => {
            const userController = getUserController(app);
            //
            // get the user to update
            //
            const user: User = await userController.findOneById(2, false);
            //
            // Change the name
            user.firstName = 'William';
            await userController.update(user.id, user);
            //
            // Ensure that it was changed
            //
            const updatedUser: User = await userController.findOneById(user.id, false);
            expect(updatedUser.firstName).toBe('William');
            //
            // Change it back
            //
            user.firstName = 'Bill';
            await userController.update(user.id, user);
            //
            // Ensure that it worked
            //
            const restoredUser: User = await userController.findOneById(user.id, false);
            expect(restoredUser.firstName).toBe('Bill');
        });

        it('should return an error if the user does not exist', async () => {
            const userController = getUserController(app);
            const user: User = new User();
            user.id = 999;
            user.firstName = 'unknown';
            user.role = 'user';
            user.password = 'who cares';
            try {
                await userController.update(user.id, user);
                fail('Valid user returned');
            }
            catch (error) {
                expect(error.status).toBe(HttpStatus.BAD_REQUEST);
                expect(error.message.message).toBe('Invalid user id');
            }
        });
    });
});