import {
    BadRequestException,
    Body,
    Controller,
    Get,
    HttpCode,
    HttpStatus,
    NotFoundException,
    Param,
    Post,
    Req,
    UseGuards,
    Delete,
} from '@nestjs/common';

import { AuthGuard } from '@nestjs/passport';
import { ParseIntPipe } from './../shared/parse-int.pipe';
// import { DeleteResult } from 'typeorm';
import { RecipeService } from './recipe.service';
import { Recipe } from './recipe.entity';
import { DeleteResult } from 'typeorm';

@Controller('recipes')
@UseGuards(AuthGuard('jwt'))
export class RecipeController {
    constructor(private readonly recipeService: RecipeService) {}

    /**
     * Return all of the user's recipes
     *
     * @param request - the request (to get the user)
     */
    @Get()
    async findAllForUser(@Req() request): Promise<Recipe[]> {
        return this.recipeService.findAllForUser(request.user.id);
    }

    @Get(':id')
    async findOneForUser(@Req() request, @Param('id', new ParseIntPipe()) id: number): Promise<Recipe> {
        const recipe: Recipe | undefined = await this.recipeService.findOneForUserById(id, request.user.id);
        if (recipe === undefined) {
            throw new NotFoundException('recipe not found');
        } else {
            return recipe;
        }
    }

    /**
     * Save an update to an existing recipe or a new recipe.
     *
     * @param request - the request containing the user
     * @param recipe - the recipe to save
     */
    @Post()
    async saveForUser(@Req() request, @Body() recipe: Recipe): Promise<Recipe | undefined> {
        const result = await this.recipeService.saveForUser(recipe, request.user.id);
        if (result === undefined) {
            throw new BadRequestException('recipe id not found or does not belong to this user');
        } else {
            return result;
        }
    }

    /**
     * Delete an existing ingredient
     * @param params should contain the ID of the user to delete
     */
    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteForUser(@Req() req, @Param('id', new ParseIntPipe()) id: number): Promise<DeleteResult> {
        return await this.recipeService.deleteForUser(id, req.user.id);
    }

    @Post(':id/addToShoppingList')
    @HttpCode(HttpStatus.NO_CONTENT)
    async addToShoppingList(@Req() request, @Param('id', new ParseIntPipe()) id: number): Promise<void> {
        if (!(await this.recipeService.addToShoppingList(id, request.user.id))) {
            throw new BadRequestException('recipe id not found or does not belong to this user');
        }
    }
}
