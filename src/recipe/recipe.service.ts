import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityManager, Repository, DeleteResult } from 'typeorm';
import { Ingredient } from '../ingredient/ingredient.entity';
import { Recipe } from './recipe.entity';

@Injectable()
export class RecipeService {

    constructor(
        @InjectRepository(Recipe)
        private readonly recipeRepository: Repository<Recipe>,
      ) {}

    /**
     * Find and return the list of existing recipes for the current user
     *
     * @param userId - the id of the user to find existing recipes for
     */
    async findAllForUser(userId: number): Promise<Recipe[]> {
        return this.recipeRepository.find({where: { userId }, relations: ['ingredients']});
    }

    /**
     * Find a specifc recpice for a specific user
     *
     * @param id - the recipe id
     * @param userId - the user id
     */
    async findOneForUserById(id: number, userId: number): Promise<Recipe | undefined> {
        return this.recipeRepository.findOne({id, userId}, {relations: ['ingredients']});
    }

    /**
     * Save an update to an existing recipe or save a new recipe.  This includes the
     * recipe's ingredients. We take the most straight foward approach to the ingrdients
     * (which are either new or possibily modified in the case of an existing recipe) and
     * delete the existing ingredients for the existing recipe and then insert the new
     * ingredients and finally save the recipe
     *
     * @param recipe to save
     */
    async saveForUser(recipe: Recipe, userId: number): Promise<Recipe | undefined> {
        let result: Recipe | undefined;

        await this.recipeRepository.manager.transaction(async (transactionManager: EntityManager): Promise<void> => {

            //
            // If this is an existing recipe, delete it's ingredients so that they can be re-inserted.
            //
            if (recipe.id !== undefined) {
                //
                // Check that this recipe belongs to the cucrrent user
                //
                if ((await transactionManager.count(Recipe, {where: { id: recipe.id, userId }})) === 0) {
                    //
                    // We can't update this recipe - it's for the wrong user
                    //
                    return undefined;
                }

                //
                // Delete the recipe's ingredients (if it has any)
                //
                await transactionManager.createQueryBuilder()
                    .delete()
                    .from(Ingredient)
                    .where('recipeId = :id', { id: recipe.id})
                    .execute();
            }

            //
            // Hold on to the ingredients, but remove them from the recipe
            //
            const ingredients: Ingredient[] = (recipe.ingredients === undefined) ? [] : recipe.ingredients;
            delete recipe.ingredients;
            delete recipe.user;

            //
            // Insert/update the recipe
            //
            recipe.userId = userId;
            result = await transactionManager.getRepository(Recipe).save(recipe);

            //
            // Prepare the ingredients to be (re)inserted
            //
            for (const ingredient of ingredients) {
                delete ingredient.id;
                delete ingredient.recipe;
                ingredient.recipeId = recipe.id;
            }

            //
            // (re)insert the ingredients
            //
            await transactionManager.getRepository(Ingredient).save(ingredients);

            recipe.ingredients = ingredients;

            result = recipe;
        });

        return result;
    }

    /**
     * Delete the recipie (and it's ingredients) for a recipe the user owns.
     *
     * @param id of the recipe to delete
     * @param userId of the recipe to delete - restricting the delete to recpies the user actually owns
     *
     */
    async deleteForUser(id: number, userId: number): Promise<DeleteResult> {
        let result: DeleteResult;
        result = new DeleteResult();
        result.raw = false;

        await this.recipeRepository.manager.transaction(async (transactionManager: EntityManager): Promise<void> => {
            const recipe = await transactionManager.getRepository(Recipe).findOne({where: {id, userId}});

            if (recipe !== undefined) {
                //
                // Delete the ingredients
                //
                await transactionManager.getRepository(Ingredient).delete( { recipeId: recipe.id });

                //
                // Now delete the recipe
                //
                result = await transactionManager.getRepository(Recipe).delete( { id: recipe.id });
            }
        });

        return result;
    }

    async addToShoppingList(id: number, userId: number): Promise<boolean> {
        if ((await this.recipeRepository.count({where: {id, userId}})) === 1) {
            await this.recipeRepository.query(
                'insert into ingredient(name, amount, userId) ' +
                'select name, amount, ?' +
                  'from ingredient where recipeId = ?',
                [userId, id]);
            return true;
        } else {
            return false;
        }
    }
}