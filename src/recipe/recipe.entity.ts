import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm';
import { Ingredient } from '../ingredient/ingredient.entity';
import { User } from '../user/user.entity';

@Entity()
export class Recipe {

    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ nullable: false })
    userId?: number;

    @ManyToOne(() => User, user => user.recipes)
    user?: User;

    @Column()
    name?: string;

    @Column()
    description?: string;

    @Column()
    imagePath?: string;

    @OneToMany(() => Ingredient, ingredient => ingredient.recipe)
    ingredients?: Ingredient[];
}