import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Ingredient } from '../ingredient/ingredient.entity';
import { User } from '../user/user.entity';
import { RecipeController } from './recipe.controller';
import { Recipe } from './recipe.entity';
import { RecipeModule } from './recipe.module';

// Code to get the usercontroller from the app
//
function getRecipeController(app: TestingModule): RecipeController {
    return app.select<RecipeModule>(RecipeModule).get<RecipeController>(RecipeController);
}

function kungPaoTest(kungPao: Recipe, result: Recipe): void {
    expect(result.name).toBe(kungPao.name);
    expect(result.description).toBe(kungPao.description);
    expect(result.imagePath).toBe(kungPao.imagePath);
    expect(result.ingredients).toHaveLength(kungPao.ingredients.length);
    for (const ki of kungPao.ingredients) {
        expect(result.ingredients).toContainEqual(ki);
    }
}

describe('RecipeController', () => {
    let app: TestingModule;
    let adam: User;
    let bill: User;
    let newRecipeId: number | undefined;

    beforeAll(async () => {
        app = await Test.createTestingModule({
            imports: [TypeOrmModule.forRoot(), RecipeModule],
        }).compile();

        const pwd = 'password123';
        adam = new User();
        adam.id = 1;
        adam.username = 'aandersen';
        adam.firstName = 'Adam';
        adam.lastName = 'Andersen';
        adam.emailAddress = 'aandersen@ces.com';
        adam.role = 'admin';
        adam.password = pwd;

        bill = new User();
        bill.id = 2;
        bill.username = 'bbaxter';
        bill.firstName = 'Bill';
        bill.lastName = 'Baxter';
        bill.emailAddress = 'bbaxter@ces.com';
        bill.role = 'user';
        bill.password = pwd;
    });

    describe('findAllForUser', () => {
        it('should return 2 recipes for user aandersen', async () => {
            const req = { user: adam };
            const recipeController = getRecipeController(app);
            const recipes = await recipeController.findAllForUser(req);
            expect(recipes).toHaveLength(2);
            const recipe1 = recipes[0];
            expect(recipe1.name).toBe('Tasty Schnitzel');
            expect(recipe1.description).toBe('A super-tasty Schnitzel - just awesome!');
            expect(recipe1.imagePath).toBe('https://upload.wikimedia.org/wikipedia/commons/7/72/Schnitzel.JPG');
            expect(recipe1.ingredients).toHaveLength(2);
            expect(recipe1.ingredients).toContainEqual(
                {
                     id: 8,
                     recipeId: 1,
                     name: 'Veal',
                     amount: 2,
                     userId: null,
                });
            expect(recipe1.ingredients).toContainEqual(
                    {
                         id: 9,
                         recipeId: 1,
                         name: 'French Fries',
                         amount: 50,
                         userId: null,
                    });
            });
    });

    describe('findOneForUser', () => {
        it('Should return a single recipe for the current user', async () => {
            const req = { user: adam };
            const recipeController = getRecipeController(app);
            const recipe1 = await recipeController.findOneForUser(req, 1);
            expect(recipe1.name).toBe('Tasty Schnitzel');
            expect(recipe1.description).toBe('A super-tasty Schnitzel - just awesome!');
            expect(recipe1.imagePath).toBe('https://upload.wikimedia.org/wikipedia/commons/7/72/Schnitzel.JPG');
            expect(recipe1.ingredients).toHaveLength(2);
            expect(recipe1.ingredients).toContainEqual(
                {
                     id: 8,
                     recipeId: 1,
                     name: 'Veal',
                     amount: 2,
                     userId: null,
                });
            expect(recipe1.ingredients).toContainEqual(
                    {
                         id: 9,
                         recipeId: 1,
                         name: 'French Fries',
                         amount: 50,
                         userId: null,
                    });

        });
    });

    describe('saveForUser', () => {
        it('Should save a new recipe for bill', async () => {
            const req = { user: bill};
            const recipeController = getRecipeController(app);

            const kungPao: Recipe = {
                name: 'Kung Pao chicken',
                description: 'A spicy schezuan favorite!',
                imagePath: 'https://www.gimmesomeoven.com/wp-content/uploads/2015/08/Kung-Pao-Chicken-1.jpg',
                user: null,
                ingredients: [
                    { name: 'chicken thighs', amount: 2 },
                    { name: 'soy sauce', amount: 1 },
                    { name: 'garlic', amount: 1 },
                    { name: 'ginger', amount: 1 },
                    { name: 'pepper corns', amount: 4 },
                ],
            };
            const saveResult: Recipe = await recipeController.saveForUser(req, kungPao);
            kungPaoTest(kungPao, saveResult);

            //
            // Now read it back and make sure it's right
            //
            const readResult: Recipe | undefined = await recipeController.findOneForUser(req, saveResult.id);
            kungPaoTest(kungPao, readResult);

            newRecipeId = readResult.id;
        });
    });

    describe('updateForuser', () => {
        it('Should update an existing recipe', async () => {
            const req = { user: bill};
            const recipeController = getRecipeController(app);
            if (newRecipeId === undefined) {
                fail('the id must be saved in the saveForUser test. Otherwise we don\'t know what recipe to use');
            }

            let kungPao: Recipe | undefined = await recipeController.findOneForUser(req, newRecipeId);
            if (kungPao === undefined) {
                fail('Unable to find previously saved recipe');
            } else {
                //
                // This should not 'look' like a recipe that was seen before. No identifying
                // properties aside from the recipe id.
                //
                kungPao = { ...kungPao };
                delete kungPao.userId;
                kungPao.ingredients = kungPao.ingredients.map( (i: Ingredient) => {
                    const newI: Ingredient = { ...i };
                    delete newI.id;
                    delete newI.recipe;
                    delete newI.recipeId;
                    delete newI.userId;
                    delete newI.user;
                    return newI;
                });

                //
                // Modify the new recipe and re-save it
                //
                kungPao.name = 'Kung Pao Chicken';
                kungPao.ingredients[1].amount = 2;
                const newIng: Ingredient = { name: 'red peppers', amount: 3 };
                kungPao.ingredients.push(newIng);

                const saveResult: Recipe = await recipeController.saveForUser(req, kungPao);
                kungPaoTest(kungPao, saveResult);

                //
                // Now read it back and make sure it's right
                //
                const readResult: Recipe | undefined = await recipeController.findOneForUser(req, saveResult.id);
                kungPaoTest(kungPao, readResult);
            }
        });
    });

    describe('addToShoppingList', () => {
        it('should add the recipe\'s ingredients to the user\'s shopping list', async () => {
            const req = { user: bill};
            const recipeController = getRecipeController(app);
            try {
                const kungPao = await recipeController.findOneForUser(req, newRecipeId);
            } catch (error) {
                fail(`unable to find new recipe: ${newRecipeId}`);
            }

            try {
                await recipeController.addToShoppingList(req, newRecipeId);
            } catch (error) {
                fail(`unable to add recipe ingredients to user\'s shopping list: ${error}`);
            }
        });
    });

    describe('deleteForuser', () => {
        it('should delete the newly added recipe', async () => {
            const req = { user: bill};
            const recipeController = getRecipeController(app);

            //
            // Delete it
            //
            await recipeController.deleteForUser(req, newRecipeId);

            //
            // Make sure it isn't there any more
            //
            try {
                await recipeController.findOneForUser(req, newRecipeId);
                fail('the recipe was not deleted');
            } catch (error) {
                expect(error.status).toBe(HttpStatus.NOT_FOUND);
                expect(error.message.message).toBe('recipe not found');
            }
        });
    });
});
