import { RecipeController } from './recipe.controller';
import { RecipeService } from './recipe.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { Recipe } from './recipe.entity';

@Module( {
    imports: [TypeOrmModule.forFeature([Recipe])],
    providers: [RecipeService],
    controllers: [RecipeController],
})
export class RecipeModule {}