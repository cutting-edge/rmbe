import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthModule } from './auth/auth.module';
import { IngredientModule } from './ingredient/ingredient.module';
import { UserModule } from './user/user.module';
import { AppController } from './app.controller';
import { RecipeModule } from './recipe/recipe.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    UserModule,
    IngredientModule,
    AuthModule,
    RecipeModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
