import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../user/user.entity';
import { IngredientController } from './ingredient.controller';
import { Ingredient } from './ingredient.entity';
import { IngredientModule } from './ingredient.module';

//
// Code to get the usercontroller from the app
//
function getIngredientController(app: TestingModule): IngredientController {
    return app.select<IngredientModule>(IngredientModule).get<IngredientController>(IngredientController);
}

//
// function for testing ingredients
function testIngredient(ingredient: Ingredient, id: number, name: string, amount: number): void {
    expect(ingredient.id).toBe(id);
    expect(ingredient.name).toBe(name);
    expect(ingredient.amount).toBe(amount);
}

describe('IngredientController', () => {

    let app: TestingModule;
    let adam: User;
    let bill: User;
    let createdIngedientId: number;

    beforeAll(async () => {
        app = await Test.createTestingModule({
            imports: [TypeOrmModule.forRoot(), IngredientModule],
        }).compile();

        const pwd = 'password123';
        adam = new User();
        adam.id = 1;
        adam.username = 'aandersen';
        adam.firstName = 'Adam';
        adam.lastName = 'Andersen';
        adam.emailAddress = 'aandersen@ces.com';
        adam.role = 'admin';
        adam.password = pwd;

        bill = new User();
        bill.id = 2;
        bill.username = 'bbaxter';
        bill.firstName = 'Bill';
        bill.lastName = 'Baxter';
        bill.emailAddress = 'bbaxter@ces.com';
        bill.role = 'user';
        bill.password = pwd;
    });

    describe('findAll', () => {
        it('should return a list of 4 ingredients for the user adam', async () => {
            const req = {user: adam};
            const ingredientController = getIngredientController(app);
            const ingredients: Ingredient[] = await ingredientController.findAllForUser(req);
            expect(ingredients).toHaveLength(4);
            //
            // Test the users
            //
            testIngredient(ingredients[0], 1, 'brussel sprouts', 40);
            testIngredient(ingredients[1], 2, 'josh', 3);
            testIngredient(ingredients[2], 3, 'potato chips', 1);
            testIngredient(ingredients[3], 4, 'cauliflower', 1);
        });
    });

    describe('findOneForUserById', () => {
        it('should return a single ingredient for the current user', async () => {
            const req = {user: adam};
            const ingredientController = getIngredientController(app);
            const ingredient: Ingredient = await ingredientController.findOneForUserById(req, 1);
            expect(ingredient.id).toBe(1);
            expect(ingredient.name).toBe('brussel sprouts');
            expect(ingredient.amount).toBe(40);
        });

        it('should fail if the ingredient id can not be found', async () => {
            const req = {user: adam};
            const ingredientController = getIngredientController(app);
            try {
                const ingredient: Ingredient = await ingredientController.findOneForUserById(req, 999);
                fail('Valid ingredient returned');
            }
            catch (error) {
                expect(error.status).toBe(HttpStatus.NOT_FOUND);
                expect(error.message.message).toBe('ingredient not found');
            }
        });

        it('should fail if the ingredient is not for this user', async () => {
            const req = {user: adam};
            const ingredientController = getIngredientController(app);
            try {
                const ingredient: Ingredient = await ingredientController.findOneForUserById(req, 5);
                fail('Valid ingredient returned');
            }
            catch (error) {
                expect(error.status).toBe(HttpStatus.NOT_FOUND);
                expect(error.message.message).toBe('ingredient not found');
            }
        });
    });

    describe('createForUser', () => {
        it('should create a new ingredient for the logged in user', async () => {
            //
            // Create an ingredient
            //
            const req = {user: bill};
            const ingredient = new Ingredient();
            ingredient.name = 'flank steak';
            ingredient.amount = 2;
            const ingredientController = getIngredientController(app);
            const createdIngredient = await ingredientController.createForUser(req, ingredient);
            expect(createdIngredient).toEqual(ingredient);

            //
            // Make sure it's there
            //
            const ing: Ingredient | undefined = await ingredientController.findOneForUserById(req, createdIngredient.id);
            expect(ing).toEqual(createdIngredient);
            createdIngedientId = ing.id;
        });
    });

    describe('updateForUser', () => {
        it('should update the amount of an ingredient (adding 5)', async () => {
            const req = {user: bill};
            const ingredientController = getIngredientController(app);
            const ingredients: Ingredient[] = await ingredientController.findAllForUser(req);
            const first = ingredients[0];
            first.amount += 5;
            await ingredientController.updateForUser(req, first.id, first);
            const firstUpdated: Ingredient = await ingredientController.findOneForUserById(req, first.id);
            expect(firstUpdated.amount).toBe(first.amount);
        });
    });

    describe('deleteForUser', () => {
        it('should delete an ingredient for the logged in user', async () => {
            const req = {user: bill};
            const ingredientController = getIngredientController(app);
            await ingredientController.deleteForUser(req, createdIngedientId);
            try {
                await ingredientController.findOneForUserById(req, createdIngedientId);
                fail('ingredient was not deleted.');
            }
            catch (error) {
                expect(error.status).toBe(HttpStatus.NOT_FOUND);
                expect(error.message.message).toBe('ingredient not found');
            }
        });
    });
});