import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { IngredientController } from './ingredient.controller';
import { IngredientService } from './ingredient.service';
import { Ingredient } from './ingredient.entity';

@Module( {
    imports: [TypeOrmModule.forFeature([Ingredient])],
    providers: [IngredientService],
    controllers: [IngredientController],
})
export class IngredientModule {}