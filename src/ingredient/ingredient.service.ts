import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Ingredient } from './ingredient.entity';

@Injectable()
export class IngredientService {
    constructor(
        @InjectRepository(Ingredient)
        private readonly ingredientRepository: Repository<Ingredient>,
    ) {}

    /**
     * Each user has a shopping list.  This returns the users shopping list.
     * @param theUser the user to return the ingredients for
     */
    async findAllForUser(userId: number): Promise<Ingredient[]> {
        return this.ingredientRepository.find({where: { userId }});
    }

    /**
     * Find the ingredient that belons to a given user.  This makes sure the
     * ingredient belongs to the use as opposed to a recipe or another user etc.
     *
     * @param id of the ingredient
     * @param theUser the user to whom the ingredient belows
     */
    async findOneForUserById(id: number, userId: number): Promise<Ingredient | undefined> {
        const ingredient = await this.ingredientRepository.findOne({where: {id, userId}});
        return ingredient;
    }

    /**
     * Create a new ingredient for a user.  In an idea that may not turn out well, this set's
     * the ingredient's user.
     *
     * @param ingredient ingredient to create
     * @param theUser user to create the ingredient for
     */
    async createForUser(ingredient: Ingredient, userId: number): Promise<Ingredient> {
        ingredient.userId = userId;
        const newIngredeint = await this.ingredientRepository.save(ingredient);
        return newIngredeint;
    }

    async updateForUser(ingredient: Ingredient, userId: number): Promise<boolean> {
        ingredient.userId = userId;
        const loadedIngredient: Ingredient | undefined = await this.ingredientRepository.preload(ingredient);
        if (loadedIngredient === undefined) {
            return false;
        } else {
            await this.ingredientRepository.save(loadedIngredient);
            return true;
        }
    }

    async deleteForUser(id: number, userId: number): Promise<DeleteResult> {
        return this.ingredientRepository.delete({ id, userId });
    }
}