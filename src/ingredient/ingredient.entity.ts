import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from '../user/user.entity';
import { Recipe } from '../recipe/recipe.entity';

@Entity()
export class Ingredient {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column('text')
    name?: string;

    @Column('int')
    amount?: number;

    @Column({ nullable: true})
    userId?: number;

    @ManyToOne(() => User, user => user.shoppingList)
    user?: User;

    @Column({ nullable: true })
    recipeId?: number;

    @ManyToOne(() => Recipe, recipe => recipe.ingredients)
    recipe?: Recipe;
}
