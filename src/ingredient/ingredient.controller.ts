import {
    BadRequestException,
    Body,
    Controller,
    Get,
    HttpCode,
    HttpStatus,
    NotFoundException,
    Param,
    Post,
    Put,
    Req,
    UseGuards,
    Delete,
} from '@nestjs/common';

import { AuthGuard } from '@nestjs/passport';
import { ParseIntPipe } from './../shared/parse-int.pipe';
import { Ingredient } from './ingredient.entity';
import { IngredientService } from './ingredient.service';
import { DeleteResult } from 'typeorm';

@Controller('ingredients')
@UseGuards(AuthGuard('jwt'))
export class IngredientController {
    constructor(private readonly ingredientService: IngredientService) {}

    /**
     * Return all of the ingredients for the user's shopping list
     *
     * @param request - the request (to get the user)
     */
    @Get()
    async findAllForUser(@Req() request): Promise<Ingredient[]> {
        return this.ingredientService.findAllForUser(request.user.id);
    }

    /**
     * Return a specific ingredient for the user
     *
     * @param request - the request (to get the user)
     * @param id - the ingredient id
     */
    @Get(':id')
    async findOneForUserById(@Req() request, @Param('id', new ParseIntPipe()) id: number): Promise<Ingredient> {
        const ingredient: Ingredient | undefined = await this.ingredientService.findOneForUserById(id, request.user.id);
        if (ingredient === undefined) {
            throw new NotFoundException('ingredient not found');
        }
        return ingredient as Ingredient;
    }

    @Post()
    @HttpCode(HttpStatus.CREATED)
    async createForUser(@Req() req, @Body() ingredient: Ingredient): Promise<Ingredient> {
        return this.ingredientService.createForUser(ingredient, req.user.id);
    }

    @Put(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    async updateForUser(@Req() req, @Param('id', new ParseIntPipe()) id: number, @Body() ingredient: Ingredient): Promise<void> {
        ingredient.id = id;
        const updated = await this.ingredientService.updateForUser(ingredient, req.user.id);
        if (!updated) {
            throw new BadRequestException('ingredient does not belong to this user');
        }
    }

    /**
     * Delete an existing ingredient
     * @param params should contain the ID of the user to delete
     */
    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteForUser(@Req() req, @Param('id', new ParseIntPipe()) id: number): Promise<DeleteResult> {
        return this.ingredientService.deleteForUser(id, req.user.id);
    }
}