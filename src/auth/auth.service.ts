import * as jwt from 'jsonwebtoken';
import { Injectable } from '@nestjs/common';

import { UserService } from '../user/user.service';
import { User } from '../user/user.entity';
import { UserAuthInfo } from '../user/user.auth.info.interface';
import { JWT } from './jwt.type';
import { UserPayload } from './user.payload.interface';

@Injectable()
export class AuthService {

  constructor(private userService: UserService) {}

  /**
   * Find the user with the auth info specified.  If we can
   * find it, then tokenize it and return it.
   *
   * @param userAuthInfo
   */
  async authenticateUser(userAuthInfo: UserAuthInfo): Promise<JWT | undefined> {

    const user: User | undefined = await this.userService.findByAuthInfo(userAuthInfo);
    if (user === undefined) {
      return undefined;
    }

    //
    // Grab the information from the user instance that we
    // want to put into the token
    //
    const userPayload: UserPayload = {
      id: user.id,
      username: user.username,
      role: user.role,
    };

    //
    // create the token and return it
    //
    const expiresIn = 60 * 60,
          secretOrKey = 'secret';
    const token: JWT = jwt.sign(userPayload, secretOrKey, { expiresIn });

    return token;
  }

  /**
   * Validate the user payload by looking the user up and checking that things didn't
   * change.
   *
   * @param user: UserPayload
   */
  async validateUser(userPayload: UserPayload): Promise<User | undefined> {
    return this.userService.findOneById(userPayload.id);
  }
}
