import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserPayload } from './user.payload.interface';
import { User } from './../user/user.entity';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super(
      {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: 'secret',
      });
  }

  public async validate(payload: UserPayload, done) {
    const user: User | undefined = await this.authService.validateUser(payload);
    if (user === undefined) {
      return done(new UnauthorizedException(), false);
    }
    done(null, user as User);
  }
}
