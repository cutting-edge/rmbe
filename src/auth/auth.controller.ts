import { Controller, Post, Get, Body, Req, HttpStatus, HttpCode, UnauthorizedException, UseGuards} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { AuthService } from './auth.service';
import { JWT } from './jwt.type';
import { UserAuthInfo } from './../user/user.auth.info.interface';
import { User } from './../user/user.entity';

interface TokenResponse {
  token: JWT;
}

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @HttpCode(HttpStatus.OK)
  public async login(@Body() userAuthInfo: UserAuthInfo): Promise<TokenResponse> {
    const jwt: JWT | undefined = await this.authService.authenticateUser(userAuthInfo);
    if (jwt === undefined) {
      throw new UnauthorizedException('Invalid user credentials');
    }
    return {token: jwt as JWT};
  }

  @Get('authorized')
  @UseGuards(AuthGuard('jwt'))
  public async authorized(@Req() request): Promise<User> {
    return request.user as User;
  }
}
