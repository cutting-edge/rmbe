import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';

@Injectable()
export class ParseBooleanPipe implements PipeTransform<string> {
  async transform(value: string | undefined, _metadata: ArgumentMetadata) {
      const val = (value === undefined) ? false : value.toLowerCase();

      return (val === 'true') || (val === 'yes');
  }
}