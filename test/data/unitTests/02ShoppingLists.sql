--
-- Shopping list for Adam
--
insert into ingredient(name, amount, userId) values('brussel sprouts', 40, 1);
insert into ingredient(name, amount, userId) values('josh', 3, 1);
insert into ingredient(name, amount, userId) values('potato chips', 1, 1);
insert into ingredient(name, amount, userId) values('cauliflower', 1, 1);

--
-- Bill has no shopping list
--

--
-- Shopping List for Cathy
--
insert into ingredient(name, amount, userId) values('hamburgers', 4, 3);
insert into ingredient(name, amount, userId) values('coke', 6, 3);
insert into ingredient(name, amount, userId) values('flank steak', 1, 3);
