--
-- Recipes for Adam
--
insert into recipe(userId, name, description, imagePath) values(
    1,
    'Tasty Schnitzel',
    'A super-tasty Schnitzel - just awesome!',
    'https://upload.wikimedia.org/wikipedia/commons/7/72/Schnitzel.JPG'
);

insert into ingredient(name, amount, recipeId) values ('Veal', 2, 1);
insert into ingredient(name, amount, recipeId) values('French Fries', 50, 1);

insert into recipe(userId, name, description, imagePath) values(
    1,
    'Big Fat Burger',
    'Just like the name says, it is big, fat and yummy!',
    'https://images.pexels.com/photos/161674/appetite-beef-big-bread-161674.jpeg'
);

insert into ingredient(name, amount, recipeId) values ('Chop Meat', 2, 2);
insert into ingredient(name, amount, recipeId) values('Cheese', 4, 2);
insert into ingredient(name, amount, recipeId) values('Buns', 1, 2);
