import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import request from 'supertest';
import { UserAuthInfo } from '../src/user/user.auth.info.interface';
import { UserModule } from '../src/user/user.module';
import { AuthModule } from './../src/auth/auth.module';
import { JWT } from './../src/auth/jwt.type';

describe('UserController (e2e)', () => {
  let app: INestApplication;
  let saveId: number;
  let authToken: JWT;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [TypeOrmModule.forRoot(), UserModule, AuthModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('protect all /user endpoints', () => {
    return request(app.getHttpServer())
      .get('/users')
      .expect(HttpStatus.UNAUTHORIZED);
  });

  it('return a valid authorization token', async () => {
    const authInfo: UserAuthInfo = {username: 'aandersen', password: 'password123'};
    const response = await request(app.getHttpServer())
      .post('/auth/login')
      .send(authInfo);
    expect(response.status).toBe(HttpStatus.OK);
    authToken = response.body.token;
  });

  it('/GET /users', () => {
    const pwd = 'password123';
    return request(app.getHttpServer())
        .get('/users')
        .set('Authorization', `Bearer ${authToken}`)
        .expect(HttpStatus.OK)
        .expect([
            {id: 1, username: 'aandersen',  firstName: 'Adam',   lastName: 'Andersen',
             role: 'admin',  emailAddress: 'aandersen@ces.com', password: pwd},
            {id: 2, username: 'bbaxter',    firstName: 'Bill',   lastName: 'Baxter',
             role: 'user',   emailAddress: 'bbaxter@ces.com', password: pwd},
            {id: 3, username: 'cconnor',    firstName: 'Cathy',  lastName: 'Connor',
             role: 'user',   emailAddress: 'cconnor@ces.com', password: pwd},
        ]);
  });

  it('/GET /users/1', () => {
    const pwd = 'password123';
    return request(app.getHttpServer())
        .get('/users/1')
        .set('Authorization', `Bearer ${authToken}`)
        .expect(HttpStatus.OK)
        .expect(
            {id: 1, username: 'aandersen',  firstName: 'Adam', lastName: 'Andersen', role: 'admin', emailAddress: 'aandersen@ces.com', password: pwd},
        );
  });

  it('/GET /users/1?deep=true', () => {
    const pwd = 'password123';
    return request(app.getHttpServer())
        .get('/users/1?deep=true')
        .set('Authorization', `Bearer ${authToken}`)
        .expect(HttpStatus.OK)
        .expect(
            {id: 1, username: 'aandersen', firstName: 'Adam', lastName: 'Andersen', role: 'admin', emailAddress: 'aandersen@ces.com',
             password: pwd, shoppingList: [
              {id: 1, name: 'brussel sprouts', amount: 40,  userId: 1, recipeId: null },
              {id: 2, name: 'josh', amount: 3,  userId: 1, recipeId: null },
              {id: 3, name: 'potato chips', amount: 1,  userId: 1, recipeId: null },
              {id: 4, name: 'cauliflower', amount: 1,  userId: 1, recipeId: null },
            ]},
        );
  });

  it('/POST /users', async () => {
    const pwd = 'password123';
    const newUser = {id: '', username: 'ddunn', firstName: 'Dave', lastName: 'Dunn', role: 'admin', emailAddress: 'ddunn@ces.com', password: pwd};
    const res = await request(app.getHttpServer())
        .post('/users')
        .set('Authorization', `Bearer ${authToken}`)
        .send(newUser);
    expect(res.status).toBe(HttpStatus.CREATED);
    const user = res.body;
    newUser.id = user.id;
    expect(user).toEqual(newUser);
    saveId = user.id;
  });

  it('/DELETE /users', () => {
    return request(app.getHttpServer())
        .delete(`/users/${saveId}`)
        .set('Authorization', `Bearer ${authToken}`)
        .expect(HttpStatus.NO_CONTENT);
  });

});
