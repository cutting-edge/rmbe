import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import request from 'supertest';
import { AuthModule } from '../src/auth/auth.module';
import { JWT } from '../src/auth/jwt.type';
import { RecipeModule } from '../src/recipe/recipe.module';
import { UserAuthInfo } from '../src/user/user.auth.info.interface';

describe('RecipeController (e2e)', () => {
    let app: INestApplication;
    let saveId: number;
    let authToken: JWT;

    beforeAll(async () => {
      const moduleFixture = await Test.createTestingModule({
        imports: [TypeOrmModule.forRoot(), AuthModule, RecipeModule],
      }).compile();

      app = moduleFixture.createNestApplication();
      await app.init();
    });

    it('protect all /recipes endpoints', () => {
        return request(app.getHttpServer())
          .get('/recipes')
          .expect(401);
    });

    it('return a valid authorization token', async () => {
        const authInfo: UserAuthInfo = {username: 'aandersen', password: 'password123'};
        const response = await request(app.getHttpServer())
            .post('/auth/login')
            .send(authInfo);
        expect(response.status).toBe(200);
        authToken = response.body.token;
    });

    it('/GET /recipes', () => {
        return request(app.getHttpServer())
            .get('/recipes')
            .set('Authorization', `Bearer ${authToken}`)
            .expect(200)
            .expect([
                {
                    id: 1,
                    userId: 1,
                    name: 'Tasty Schnitzel',
                    description: 'A super-tasty Schnitzel - just awesome!',
                    imagePath: 'https://upload.wikimedia.org/wikipedia/commons/7/72/Schnitzel.JPG',
                    ingredients: [
                        {
                            id: 9,
                            name: 'French Fries',
                            amount: 50,
                            recipeId: 1,
                            userId: null,
                        },
                        {
                            id: 8,
                            name: 'Veal',
                            amount: 2,
                            userId: null,
                            recipeId: 1,
                        },
                    ],
                },
                {
                    id: 2,
                    userId: 1,
                    name: 'Big Fat Burger',
                    description: 'Just like the name says, it is big, fat and yummy!',
                    imagePath: 'https://images.pexels.com/photos/161674/appetite-beef-big-bread-161674.jpeg',
                    ingredients: [
                        {
                            id: 12,
                            name: 'Buns',
                            amount: 1,
                            recipeId: 2,
                            userId: null,
                        },
                        {
                            id: 11,
                            name: 'Cheese',
                            amount: 4,
                            recipeId: 2,
                            userId: null,
                        },
                        {
                            id: 10,
                            name: 'Chop Meat',
                            amount: 2,
                            recipeId: 2,
                            userId: null,
                        },
                    ],
                },
            ]);
    });

    it('return a valid authorization token for bill baxter', async () => {
        const authInfo: UserAuthInfo = {username: 'bbaxter', password: 'password123'};
        const response = await request(app.getHttpServer())
            .post('/auth/login')
            .send(authInfo);
        expect(response.status).toBe(200);
        authToken = response.body.token;
    });

    it('/POST /recipes', async () => {
        let pho = {
            name: 'Pho',
            description: 'Classic Vietnamese staple',
            imagePath: 'https://upload.wikimedia.org/wikipedia/commons/5/53/Pho-Beef-Noodles-2008.jpg',
            ingredients: [
                { name: 'beef brisket', amount: 2 },
                { name: 'tendon', amount: 2 },
                { name: 'tripe', amount: 1 },
                { name: 'rice noodles', amount: 20},
            ],
        };
        let res = await request(app.getHttpServer())
            .post('/recipes')
            .set('Authorization', `Bearer ${authToken}`)
            .send(pho);

        expect(res.status).toBe(HttpStatus.CREATED);
        let result = res.body;
        expect(result.name).toBe(pho.name);
        expect(result.description).toBe(pho.description);
        expect(result.ingredients).toHaveLength(pho.ingredients.length);
        let resIng1 = result.ingredients[0];
        let phoIng1 = pho.ingredients[0];
        expect(resIng1.name).toBe(phoIng1.name);
        expect(resIng1.amount).toBe(phoIng1.amount);

        pho = result;
        pho.name = 'Beef Pho';
        pho.ingredients[1].amount = 1;
        res = await request(app.getHttpServer())
            .post('/recipes')
            .set('Authorization', `Bearer ${authToken}`)
            .send(pho);
        expect(res.status).toBe(HttpStatus.CREATED);
        result = res.body;
        expect(result.name).toBe(pho.name);
        expect(result.description).toBe(pho.description);
        expect(result.ingredients).toHaveLength(pho.ingredients.length);
        resIng1 = result.ingredients[0];
        phoIng1 = pho.ingredients[0];
        expect(resIng1.name).toBe(phoIng1.name);
        expect(resIng1.amount).toBe(phoIng1.amount);
        saveId = result.id;
    });

    it('/POST /recipes/id/addToShoppingList', async () => {
        return request(app.getHttpServer())
        .post(`/recipes/${saveId}/addToShoppingList`)
        .set('Authorization', `Bearer ${authToken}`)
        .expect(HttpStatus.NO_CONTENT);
    });

    it('/DELETE /recipes', async () => {
        return request(app.getHttpServer())
        .delete(`/recipes/${saveId}`)
        .set('Authorization', `Bearer ${authToken}`)
        .expect(HttpStatus.NO_CONTENT);
    });
});
