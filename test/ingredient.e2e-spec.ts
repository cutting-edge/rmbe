import request from 'supertest';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { INestApplication, Delete, HttpStatus } from '@nestjs/common';

import { AuthModule } from './../src/auth/auth.module';
import { IngredientModule } from './../src/ingredient/ingredient.module';
import { Ingredient } from './../src/ingredient/ingredient.entity';
import { JWT } from './../src/auth/jwt.type';
import { UserAuthInfo } from './../src/user/user.auth.info.interface';

describe('IngredientController (e2e)', () => {
    let app: INestApplication;
    let saveId: number;
    let authToken: JWT;

    beforeAll(async () => {
      const moduleFixture = await Test.createTestingModule({
        imports: [TypeOrmModule.forRoot(), AuthModule, IngredientModule],
      }).compile();

      app = moduleFixture.createNestApplication();
      await app.init();
    });

    it('protect all /ingredients endpoints', () => {
      return request(app.getHttpServer())
        .get('/ingredients')
        .expect(401);
    });

    it('return a valid authorization token', async () => {
        const authInfo: UserAuthInfo = {username: 'aandersen', password: 'password123'};
        const response = await request(app.getHttpServer())
            .post('/auth/login')
            .send(authInfo);
        expect(response.status).toBe(200);
        authToken = response.body.token;
    });

    it('/GET /ingredients', () => {
        return request(app.getHttpServer())
            .get('/ingredients')
            .set('Authorization', `Bearer ${authToken}`)
            .expect(200)
            .expect([
                { id: 1, name: 'brussel sprouts',   amount: 40, userId: 1, recipeId: null },
                { id: 2, name: 'josh',              amount: 3,  userId: 1, recipeId: null },
                { id: 3, name: 'potato chips',      amount: 1,  userId: 1, recipeId: null },
                { id: 4, name: 'cauliflower',       amount: 1,  userId: 1, recipeId: null },
            ]);
    });

    it('/GET /ingredients/1', () => {
        return request(app.getHttpServer())
            .get('/ingredients/1')
            .set('Authorization', `Bearer ${authToken}`)
            .expect(HttpStatus.OK)
            .expect(
                {id: 1, name: 'brussel sprouts',  amount: 40, userId: 1, recipeId: null},
            );
      });

    it('/POST /ingredients', async () => {
        //
        // The new ingredient should *not* specify the user or recipie id's
        //
        const newIngredient = { id: null, name: 'new york strip steaks', amount: 5,  userId: null, recipeId: null };
        const res = await request(app.getHttpServer())
            .post('/ingredients')
            .set('Authorization', `Bearer ${authToken}`)
            .send(newIngredient);
        expect(res.status).toBe(HttpStatus.CREATED);
        const ingredient = res.body;
        newIngredient.id = ingredient.id;
        newIngredient.userId = 1; // aandersen's id
        expect(ingredient).toEqual(newIngredient);
        saveId = ingredient.id;
    });

    it('/PUT /ingredients', async () => {
        //
        // Get the newly created ingredient
        //
        let res = await request(app.getHttpServer())
            .get(`/ingredients/${saveId}`)
            .set('Authorization', `Bearer ${authToken}`)
            .send();

        expect(res.status).toBe(HttpStatus.OK);
        const ingredient = res.body;
        ingredient.amount += 10;

        await request(app.getHttpServer())
            .put(`/ingredients/${saveId}`)
            .set('Authorization', `Bearer ${authToken}`)
            .send(ingredient)
            .expect(HttpStatus.NO_CONTENT);

        res = await request(app.getHttpServer())
            .get(`/ingredients/${saveId}`)
            .set('Authorization', `Bearer ${authToken}`)
            .send();
        expect(res.status).toBe(HttpStatus.OK);
        expect(res.body).toEqual(ingredient);
    });

    it('/DELETE /ingredients', () => {
        return request(app.getHttpServer())
            .delete(`/ingredients/${saveId}`)
            .set('Authorization', `Bearer ${authToken}`)
            .expect(HttpStatus.NO_CONTENT);
    });

});