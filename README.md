# Recipe Manager Backend

A [Nest](https://github.com/nestjs/nest) backend for the [Angular 6 Complete Guide course on UDemy](https://www.udemy.com/the-complete-guide-to-angular-2/).  You can find the NGXS implementation of the couse project that works with this backend [here](https://bitbucket.org/rich_duncan/rmng/src/master/)

## Installation

```bash
$ npm install
```
## Setting up the database

The project currently uses sqlite3 as a database. The database is stored in the file _db.sqlite_. The database can be recreated/reinitialized by running
the gulp command:

```bash
$ gulp prepare-unit-test-db
```
This performs the following actions:


* Delete the current database file
* Re-create the file based on the typeorm entities and relations found in the project (using the typeorm cli).
* Load the unit test data.  These are sql files in the ./test/data folder.  Note the ordering of the files (by filename).

## Running the server

```bash
# development mode - reruns the server each time files are changed
$ npm run start

# watch mode
$ npm run start:dev

# production mode
npm run start:prod
```

## Testing

The testing is done with Gulp which has tasks to reset the (sqlite3) database.

```bash
# unit tests
$ gulp unit-test

# e2e tests
$ gulp e2e-test

# test coverage
$ npm run test:cov
```

## Other Gulp workflows

```bash
# lint and compile during development
$ gulp watch

```