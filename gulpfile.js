var gulp = require("gulp");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");
var clean = require("gulp-clean");
var tslint = require("gulp-tslint");
var run = require("gulp-run");
var sql = require("gulp-sqlite3");
var print = require("gulp-print").default
var gulpSequence = require('gulp-sequence')
var watch = require('gulp-watch')


gulp.task("compile", function () {
    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest("dist"));
});

gulp.task("clean", function() {
    return gulp.src('dist', {read: false}).pipe(clean());
});

gulp.task("lint", function() {
    return gulp.src("src/**/*.ts")
        .pipe(tslint({
            formatter: "verbose"
        }))
        .pipe(tslint.report());
});

gulp.task("clean-db", function() {
    return gulp.src('db.sqlite', {read: false}).pipe(clean());
});

gulp.task("prep-db", function() {
    return run('./typeorm schema:sync').exec();
});

gulp.task("load-unit-test-db", function() {
    return gulp.src('test/data/unitTests/*.sql').pipe(sql("db.sqlite"));
});

gulp.task("run-unit-tests", function() {
    return run('npm run test').exec();
});

gulp.task("run-e2e-tests", function() {
    return run('npm run test:e2e').exec();
});

//
// watches
//
gulp.task("watch", function() {
    return gulp.watch('src/**/*.ts', ['lint', 'compile']);
});

//
// Meta tasks
//
gulp.task("build", gulpSequence(
    "clean", 
    "lint", 
    "compile"
));

gulp.task("prepare-unit-test-db", gulpSequence(
    "clean-db",
    "prep-db",
    "load-unit-test-db"
));

gulp.task("unit-test", gulpSequence(
    "build", 
    "prepare-unit-test-db",
    "run-unit-tests"
));

gulp.task("e2e-test", gulpSequence(
    "build", 
    "prepare-unit-test-db",
    "run-e2e-tests"
));
